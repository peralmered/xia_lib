#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

###############################################################################################
###############################################################################################
## ToDo

https://bitbucket.org/peralmered/xia_lib/issues?status=new&status=open

## ToDo
###############################################################################################
###############################################################################################
## Documentation

Installing Pillow for pypy:
  pypy -m pip install --use-wheel pillow


## Documentation
###############################################################################################
###############################################################################################
## Notes



## Notes
###############################################################################################
###############################################################################################


"""


boolAllowScreenOutput=True

###############################################################################################
###############################################################################################
## Defines and constants


NO_ERROR=0
numErrBit=1
FILE_EXISTS_FAIL=numErrBit
numErrBit*=2
FILE_READABLE_FAIL=numErrBit
numErrBit*=2


## Defines and constants
################################################################################################
################################################################################################
## Globals


COLOR_FORMAT_STE="ste"
COLOR_FORMAT_TT="tt"
COLOR_FORMAT_FALCON="falcon"
COLOR_FORMAT_NUMERIC_STE=0
COLOR_FORMAT_NUMERIC_TT=1
COLOR_FORMAT_NUMERIC_FALCON=2


## Globals
###############################################################################################
###############################################################################################
## Functions


#------------------------------------------------------------------------------


def CheckFileExistsAndReadable(strFile):
  numOut=0
  boolHit=False
  if os.path.exists(strFile) and os.path.isfile(strFile):
    boolHit=True
  if not boolHit:
    numOut+=FILE_EXISTS_FAIL
  boolHit=False
  if os.access(strFile, os.R_OK):
    boolHit=True
  if not boolHit:
    numOut+=FILE_READABLE_FAIL
  return numOut


def GetBinaryFile(strFile):
  # Returns contents of binary file in array of ints, where each int represents a byte
  arrFile=[]
  byteThis=open(strFile, "rb").read()
  for b in byteThis:
    arrFile.append(ord(b))
  return arrFile


def GetTextFile(strFile):
  # Return contents of text file in array of strings, where each string is a line in the text file
  arrFile=open(strFile, "r").readlines()
  return arrFile


def WriteBinaryFile(arrData, strFile):
  # Writes binary data from arrData to binary file
  # Binary data is expected as an array of ints, where each int represents a byte
  fileOut=open(strFile, "wb")
  arrByteData=bytearray(arrData)
  fileOut.write(arrByteData)
  fileOut.close()


def WriteTextFile(arrData, strFile):
  # Writes data from arrData to textfile
  # Data is expected as an array of strings, where each string represents a line of text
  fileOut=open(strFile, "w")
  for strThis in arrData:
    fileOut.write(strThis+"\n")
  fileOut.close()


#------------------------------------------------------------------------------


def Out(str):
  global boolAllowScreenOutput
  if boolAllowScreenOutput:
    sys.stdout.write(str)


#------------------------------------------------------------------------------


def Fatal(str):
  # Fatal errors always output
  boolAllowScreenOutput=True
  Out("\nFATAL ERROR: "+str+"\n")
  exit(1)


def Warn(str):
  #boolAllowScreenOutput=True
  Out("WARNING: "+str+"\n")


def Notice(str):
  #boolAllowScreenOutput=True
  Out("NOTICE: "+str+"\n")


#------------------------------------------------------------------------------


def Exe(arrThis):
  return subprocess.check_output(arrThis)


def ExeWithOutput(arrExe):
  process = subprocess.Popen(
      arrExe, stdout=subprocess.PIPE, stderr=subprocess.PIPE
  )
  while True:
    out = process.stdout.read(1)
    if out == '' and process.poll() != None:
      break
    if out != '':
      if out=="\n":
        sys.stdout.write("\n")
        sys.stdout.flush()
      elif out=="\r":
        sys.stdout.write("\r")
        sys.stdout.flush()
      else:
        sys.stdout.write(out)
        #sys.stdout.flush()
  sys.stdout.write("\n")
  sys.stdout.flush()


#------------------------------------------------------------------------------


def StartsWith(strThis, strSub):
  return strThis[:len(strSub)]==strSub


#------------------------------------------------------------------------------


def EndsWith(strThis, strSub):
  return strThis[-len(strSub):]==strSub


#------------------------------------------------------------------------------


def StrToBool(n):
  if n=="True":
    return True
  return False


#------------------------------------------------------------------------------


def IsOdd(n):
  return bool(n%2)


def IsEven(n):
  return not bool(n%2)


#------------------------------------------------------------------------------


ZXRandSeed=3.14159265358979

def ZXRand():
  # Lehmer random number generator, this is the algorithm used by Sinclair in the ZX Spectrum Basic
  # http://en.wikipedia.org/wiki/Lehmer_random_number_generator
  # Requires an int in global scope named $ZXRandSeed - set this to a unique value per project
  # Returns a number between 0 and 1, will NEVER reach 1 but sometimes 0
  global ZXRandSeed
  ZXRandSeed=((int)(((ZXRandSeed+1)*75)-1)%65537.0)
  return ZXRandSeed/65537.0


def ZXRandSpan(numMin,numMax):
  # Returns a random number between numMin and numMax, with a slight weighting FROM $numMax
  # Expects ints, but should work ok with decimals too
  boolDebug=False
  if(numMax<numMin):
    numTemp=numMin
    numMin=numMax
    numMax=numTemp
  if boolDebug:
    print("--ZXRandSpan("+str(numMin)+","+str(numMax)+")=")
  numSpan=numMax-numMin
  return numMin+(ZXRand()*numSpan)


#------------------------------------------------------------------------------


numTimingClock=0
def TimingStart():
  global numTimingClock
  numTimingClock=time.clock()


def GetTimingEnd():
  return time.clock()-numTimingClock


#------------------------------------------------------------------------------


def CorrectNeg(numVal, numBits):
  numOut=0
  if numVal>=0:
    numOut=numVal
  else:
    numVal=abs(numVal)
    strBin=bin(numVal)[2:].zfill(numBits)
    #print(strBin)
    strComp=""
    for thisBit in strBin:
      if thisBit=="0":
        strComp+="1"
      else:
        strComp+="0"
    numComp=int(strComp,2)
    numComp+=1
    #print(bin(numComp)[2:].zfill(32))
    numOut=numComp
  return numOut


def MakeDevpacByteUnsigned(n):
  strN=hex(n)
  strN=strN.lstrip("0x")
  strN=strN.rstrip("L")
  strN=strN.zfill(2)
  return "$"+strN


def MakeDevpacWord(n):
  n=CorrectNeg(n, 16)
  strN=hex(n)
  strN=strN.lstrip("0x")
  strN=strN.rstrip("L")
  strN=strN.zfill(4)
  return "$"+strN


def MakeDevpacLongword(n):
  n=CorrectNeg(n, 32)
  strN=hex(n)
  strN=strN.lstrip("0x")
  strN=strN.rstrip("L")
  strN=strN.zfill(8)
  return "$"+strN


#------------------------------------------------------------------------------


def ComponentToNybble(n):
  strN=GetStringNybble(n)
  strA=strN[3]
  strB=strN[:3]
  return int(eval("0b"+strA+strB))


def ComponentToTTNybble(n):
  strN=GetStringNybble(n)
  return int(eval("0b"+strN))


def MakeSteCol(arrCol):
  # Maybe this should be replaced by arrCol[x]/17?
  r=arrCol[0]>>4
  g=arrCol[1]>>4
  b=arrCol[2]>>4
  finalCol=ComponentToNybble(r)<<8
  finalCol+=ComponentToNybble(g)<<4
  finalCol+=ComponentToNybble(b)
  return finalCol


def MakeTTCol(arrCol):
  # Maybe this should be replaced by arrCol[x]/17?
  r=arrCol[0]>>4
  g=arrCol[1]>>4
  b=arrCol[2]>>4
  finalCol=ComponentToTTNybble(r)<<8
  finalCol+=ComponentToTTNybble(g)<<4
  finalCol+=ComponentToTTNybble(b)
  return finalCol


def MakeFalconCol(arrCol):
  r=arrCol[0]&0b11111100
  g=arrCol[1]&0b11111100
  b=arrCol[2]&0b11111100
  finalCol=r<<24
  finalCol+=g<<16
  finalCol+=b
  return finalCol


def MakeFormattedColor(col, strColorFormat):
  if strColorFormat==COLOR_FORMAT_STE:
    numCol=MakeSteCol(col)
  elif strColorFormat==COLOR_FORMAT_TT:
    numCol=MakeTTCol(col)
  elif strColorFormat==COLOR_FORMAT_FALCON:
    numCol=MakeFalconCol(col)
  return numCol  


def Make8bitColorFromSteColor(numCol):
  r=numCol&0b0000111100000000
  g=numCol&0b0000000011110000
  b=numCol&0b0000000000001111
  r=r>>8
  g=g>>4
  r=GetStringNybble(r)
  g=GetStringNybble(g)
  b=GetStringNybble(b)
  tempCol=[r, g, b]
  arrCol=[]
  for thisCol in tempCol:
    strFixedCol=thisCol[1:]+thisCol[0]
    #print("thisCol="+thisCol+" ==> "+strFixedCol)
    numComp=int(eval("0b"+strFixedCol))
    numComp*=17
    arrCol.append(numComp)
  numOutCol=0
  numShift=16
  for thisCol in arrCol:
    numOutCol+=thisCol<<numShift
    numShift-=8
  return numOutCol


#------------------------------------------------------------------------------


def GetStringNybble(n):
  strNybble=str(bin(n))
  strNybble=strNybble.lstrip("0b").zfill(4)
  return strNybble


def GetStringByte(n):
  strByte=str(bin(n))
  strByte=strByte.lstrip("0b").zfill(8)
  return strByte


def ToSignedWord(val):
  return val%65536


def FromSignedWord(val):
  if val>32767:
    return -(65536-val)
  return val


#------------------------------------------------------------------------------


def Bitplanes(arrIn, numImageWidth, numImageHeight, numScreenBitplanes, numImageBitplanes):
# In: arrIn - list of ints representing chunky pixels 0-255
  numWordWidth=numImageWidth/16
  numImagePos=0
  arrAtariImage=[]
  for y in range(numImageHeight):
    for word in range(numWordWidth):
      bitmask=32768
      arrBitplanes=[0]*8
      for n in range(16):
        thisPixel=arrIn[numImagePos]
        numImagePos+=1
        strPixel=GetStringByte(thisPixel)
        for i in range(8):
          if strPixel[7-i]=="1":
            arrBitplanes[i]+=bitmask
        bitmask>>=1
      for i in range(numImageBitplanes):
        numWord=arrBitplanes[i]
        numHighByte=(numWord>>8)&0xff
        numLowByte=numWord&0xff
        arrAtariImage.append(numHighByte)
        arrAtariImage.append(numLowByte)
      for i in range(numScreenBitplanes-numImageBitplanes):
        arrAtariImage.append(0)
        arrAtariImage.append(0)
  return arrAtariImage


#------------------------------------------------------------------------------


def Make68kSourceFromArray(arrData, numDataSize, numValsPerLine=16):
  arrSource=[]
  if numDataSize==1:
    strDataSize="b"
    numMaxVal=pow(2,8)
  elif numDataSize==2:
    strDataSize="w"
    numMaxVal=pow(2,16)
  elif numDataSize==4:
    strDataSize="l"
    numMaxVal=pow(2,32)
  else:
    Fatal("Unknown datasize \""+str(numDataSize)+"\" sent to Make68kSourceFromArray()!")
  numValsInLine=0
  strLineTemplate="  dc."+strDataSize+" "
  strLine=strLineTemplate
  for v in arrData:
    if str(int(v))!=str(v):
      Fatal("Illegal value \""+str(v)+"\"!")
    if v>numMaxVal or v<-(numMaxVal>>1):
      print("numMaxVal: "+str(numMaxVal)+", -numMaxVal>>1: "+str(-(numMaxVal>>1)))
      Fatal("Illegal value \""+str(v)+"\" using dc."+strDataSize+"!")
    strLine+=str(v)
    numValsInLine+=1
    if numValsInLine<numValsPerLine:
      strLine+=", "
    else:
      arrSource.append(strLine)
      strLine=strLineTemplate
      numValsInLine=0

  if numValsInLine>0:
    if EndsWith(strLine, ", "):
      strLine=strLine[:-2]
    arrSource.append(strLine)
  return arrSource


#------------------------------------------------------------------------------


def translate(value, leftMin, leftMax, rightMin, rightMax):
# Found at: https://stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
  # Figure out how 'wide' each range is
  leftSpan = leftMax - leftMin
  rightSpan = rightMax - rightMin

  # Convert the left range into a 0-1 range (float)
  valueScaled = float(value - leftMin) / float(leftSpan)

  # Convert the 0-1 range into a value in the right range.
  return rightMin + (valueScaled * rightSpan)


#------------------------------------------------------------------------------


def ConvertHexColor(strCol):
# In: color as string "rrggbb" (hex) or "#rrggbb"
# Out: pygame-compatible tuple
  if strCol[:1]=="#":
    strCol=strCol[1:]
  strR=strCol[:2]
  strG=strCol[2:4]
  strB=strCol[4:]
  numR=int(strR, 16)
  numG=int(strG, 16)
  numB=int(strB, 16)
  return (numR, numG, numB)


#------------------------------------------------------------------------------


## Functions
###############################################################################################
###############################################################################################
## Classes


###############################################################################################
## cVec2D - 2D vector


class cVec2D():
  
  def __init__(self, numX, numY):
    self.x=numX
    self.y=numY


  #----------------------------------------------------------------------------


  def GetMag(self):
    return math.sqrt(pow(self.x, 2) + pow(self.y, 2))


  #----------------------------------------------------------------------------


  def SetMag(self, mag):
    numAngle=self.GetAngle(self)
    numMag=mag
    self.x=math.cos(numAngle)*numMag
    self.y=math.sin(numAngle)*numMag


  #----------------------------------------------------------------------------


  def Normalize(self):
    self.SetMag(1.0)


  #----------------------------------------------------------------------------


  def GetAngle(self):
    return math.atan2(self.y, self.x)


  #----------------------------------------------------------------------------


  def SetAngle(self, numAngle):
    numMag=self.GetMag()
    self.x=math.cos(numAngle)*numMag
    self.y=math.sin(numAngle)*numMag


  #----------------------------------------------------------------------------


  def Add(self, vec):
    if vec.__class__.__name__!="cVec2D":
      Fatal("cVec2D.Add() needs a cVec2D instance as parameter!")
    self.x+=vec.x
    self.y+=vec.y
    return cVec2D(self.x, self.y)
    

  #----------------------------------------------------------------------------


  def Sub(self, vec):
    if vec.__class__.__name__!="cVec2D":
      Fatal("cVec2D.Sub() needs a cVec2D instance as parameter!")
    self.x-=vec.x
    self.y-=vec.y
    return cVec2D(self.x, self.y)
    

  #----------------------------------------------------------------------------


  def Mul(self, numVal):
    numAngle=self.GetAngle()
    numMag=self.GetMag()
    x=math.cos(numAngle)*numMag*numVal
    y=math.sin(numAngle)*numMag*numVal
    return cVec2D(x, y)
    

  #----------------------------------------------------------------------------


  def Dot(self, vec):
    if vec.__class__.__name__!="cVec2D":
      Fatal("cVec2D.Dot() needs a cVec2D instance as parameter!")
    return (self.x * vec.x) + (self.y * vec.y)
    

  #----------------------------------------------------------------------------


## cVec2D - 2D vector
###############################################################################################


## Classes
###############################################################################################
###############################################################################################

import sys
import os
import time
import copy

import math

# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
  # call the main function
  Notice("This module isn't meant to be called on its own, entering self-test mode")


  boolTestMode=True
  if boolTestMode:
    #      12345678901234567890123456789012345678901234567890123456789012345678901234567890
    print("---[ Self-test mode ]---------------------------------------------------------")

    # Test STe-to-8bit-color conversion
    #      12345678901234567890123456789012345678901234567890123456789012345678901234567890
    print(" ")
    print("------------------------------------------")
    print("-- STe to 8-bit color conversion")

    arrCols=[0xfba, 0x6ca, 0x7af, 0xfff, 
             0x900, 0xe00, 0x7db, 0xdb2,
             0x715, 0xf4d, 0x703, 0x7be,
             0x429, 0x7e6, 0xf81, 0x799]
    arrColsCorrect=[0xff7755, 0xcc9955, 0xee55ff, 0xffffff,
                    0x330000, 0xdd0000, 0xeebb77, 0xbb7744,
                    0xee22aa, 0xff88bb, 0xee0066, 0xee77dd,
                    0x884433, 0xeeddcc, 0xff1122, 0xee3333]
    numErrors=0
    print(" ")
    for i, thisCol in enumerate(arrCols):
      num8bitCol=Make8bitColorFromSteColor(thisCol)
      numCorrectCol=arrColsCorrect[i]
      boolError=False
      if num8bitCol!=numCorrectCol:
        numErrors+=1
        print("ERROR: STe color "+hex(thisCol)+" ==> 8-bit color "+hex(num8bitCol)+" (correct is "+hex(numCorrectCol)+")")
    print("Results: "+str(numErrors)+" errors")
    print(" ")

    print("-- STe to 8-bit color conversion")
    print("------------------------------------------")


